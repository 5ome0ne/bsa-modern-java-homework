package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		if (prices == null) {
			return 0;
		}
		ArrayList<Integer> list = prices.filter(p -> p > 0).collect(Collectors.toCollection(ArrayList::new));
		int profit = 0;
		if (list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				profit = Math.max(bestBuy(list, i), profit);
			}
		}
		return profit;
	}

	private static int bestBuy(ArrayList<Integer> prices, int index) {
		int max = 0;
		for (int i = index + 1; i < prices.size(); i++) {
			max = Math.max((-prices.get(index) + bestSell(prices, i)), max);
		}
		return max;
	}

	private static int bestSell(ArrayList<Integer> prices, int index) {
		int max = prices.get(index);
		for (int i = index + 1; i < prices.size() - 1; i++) {
			max = Math.max(prices.get(index) + bestBuy(prices, i), max);
		}
		return max;
	}

}
