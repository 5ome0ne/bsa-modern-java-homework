package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		if (isLibraryCorrupted(libraries)) {
			return true;
		}
		boolean cycled = false;
		HashMap<String, List<String>> dependenciesMap = dependenciesToMap(libraries.dependencies);
		for (String library : libraries.libraries) {
			cycled = isLibraryCycled(dependenciesMap, library);
			if (cycled) {
				break;
			}
		}
		return !cycled;
	}

	private static boolean isLibraryCorrupted(DependencyList libraries) {
		return libraries == null || libraries.libraries == null || libraries.dependencies == null
				|| libraries.libraries.size() == 0 || libraries.dependencies.size() == 0;
	}

	private static boolean isLibraryCycled(HashMap<String, List<String>> dependenciesMap, String library) {
		boolean cycled = false;
		if (dependenciesMap.containsKey(library)) {
			List<String> currentDependencies = dependenciesMap.get(library);
			for (String depend : currentDependencies) {
				cycled = isCycled(dependenciesMap, depend, library);
				if (cycled) {
					break;
				}
			}
		}
		return cycled;
	}

	private static boolean isCycled(HashMap<String, List<String>> dependenciesMap, String currentLib, String startLib) {
		boolean cycled = startLib.equals(currentLib);
		if (cycled) {
			return true;
		}
		List<String> curLibs = dependenciesMap.getOrDefault(currentLib, null);
		if (curLibs == null) {
			return false;
		}
		for (String curLib : curLibs) {
			cycled = isCycled(dependenciesMap, curLib, startLib);
			if (cycled) {
				break;
			}
		}
		return cycled;
	}

	private static HashMap<String, List<String>> dependenciesToMap(List<String[]> dependencies) {
		HashMap<String, List<String>> dependenciesMap = new HashMap<>();
		for (String[] dependency : dependencies) {
			List<String> dependenciesList = dependenciesMap.containsKey(dependency[0])
					? dependenciesMap.get(dependency[0]) : new ArrayList<>();
			dependenciesList.add(dependency[1]);
			dependenciesMap.put(dependency[0], dependenciesList);
		}
		return dependenciesMap;
	}

}
