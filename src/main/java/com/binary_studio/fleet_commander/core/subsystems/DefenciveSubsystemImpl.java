package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private static final Integer MAX_REDUCTION = 95;

	private String name;

	private PositiveInteger powergridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		validationDefenciveSubsystemParameters(name, impactReductionPercent);
		DefenciveSubsystemImpl defenciveSubsystem = new DefenciveSubsystemImpl();
		defenciveSubsystem.name = name;
		defenciveSubsystem.powergridConsumption = powergridConsumption;
		defenciveSubsystem.capacitorConsumption = capacitorConsumption;
		defenciveSubsystem.impactReductionPercent = PositiveInteger
				.of(Math.min(impactReductionPercent.value(), MAX_REDUCTION));
		defenciveSubsystem.shieldRegeneration = shieldRegeneration;
		defenciveSubsystem.hullRegeneration = hullRegeneration;
		return defenciveSubsystem;
	}

	private static void validationDefenciveSubsystemParameters(String name, PositiveInteger impactReductionPercent) {
		if (isBlankString(name)) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
	}

	private static boolean isBlankString(String string) {
		return string == null || string.trim().isEmpty();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		PositiveInteger reduceDamage = PositiveInteger.of((int) Math.max(1,
				Math.ceil(incomingDamage.damage.value() * (1.0 - this.impactReductionPercent.value() / 100.0))));

		incomingDamage = new AttackAction(reduceDamage, incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);

		return incomingDamage;
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
