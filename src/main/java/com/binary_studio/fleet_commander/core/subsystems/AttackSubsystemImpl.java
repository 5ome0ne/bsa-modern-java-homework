package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger powergridRequirments;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		validationAttackSubsystemParameters(name);
		AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
		attackSubsystem.name = name;
		attackSubsystem.powergridRequirments = powergridRequirments;
		attackSubsystem.capacitorConsumption = capacitorConsumption;
		attackSubsystem.optimalSpeed = optimalSpeed;
		attackSubsystem.optimalSize = optimalSize;
		attackSubsystem.baseDamage = baseDamage;
		return attackSubsystem;
	}

	private static void validationAttackSubsystemParameters(String name) {
		if (isBlankString(name)) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
	}

	private static boolean isBlankString(String string) {
		return string == null || string.trim().isEmpty();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = (target.getSize().value() >= this.optimalSize.value() ? 1
				: (double) target.getSize().value() / this.optimalSize.value());
		double speedReductionModifier = (target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: this.optimalSpeed.value() / (2.0 * target.getCurrentSpeed().value()));
		int damage = (int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
