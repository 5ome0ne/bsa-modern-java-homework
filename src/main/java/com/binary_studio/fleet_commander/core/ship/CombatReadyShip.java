package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private final DockedShip ship;

	public CombatReadyShip(DockedShip dockedShip) {
		this.ship = dockedShip;
	}

	@Override
	public void endTurn() {
		this.ship.capacitorRecharge();
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getCurrentSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.ship.changeCapacitorAmount(-this.ship.getAttackSubsystem().getCapacitorConsumption().value())) {
			PositiveInteger damage = this.ship.getAttackSubsystem().attack(target);
			AttackAction attackAction = new AttackAction(damage, this, target, this.ship.getAttackSubsystem());
			return Optional.of(attackAction);
		}
		return Optional.empty();
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		return this.ship.applyAttack(attack);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.ship.changeCapacitorAmount(-this.ship.getDefenciveSubsystem().getCapacitorConsumption().value())) {
			return this.ship.shieldRegenerate();
		}
		return Optional.empty();
	}

}
