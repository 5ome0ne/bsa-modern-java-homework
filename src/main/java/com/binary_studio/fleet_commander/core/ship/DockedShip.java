package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger shieldHPMax;

	private PositiveInteger hullHP;

	private PositiveInteger hullHPMax;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorAmountMax;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger powergridOutput;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		DockedShip dockedShip = new DockedShip();
		dockedShip.name = name;
		dockedShip.shieldHP = shieldHP;
		dockedShip.shieldHPMax = shieldHP;
		dockedShip.hullHP = hullHP;
		dockedShip.hullHPMax = hullHP;
		dockedShip.powergridOutput = powergridOutput;
		dockedShip.capacitorAmount = capacitorAmount;
		dockedShip.capacitorAmountMax = capacitorAmount;
		dockedShip.capacitorRechargeRate = capacitorRechargeRate;
		dockedShip.speed = speed;
		dockedShip.size = size;
		return dockedShip;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			removeAttackSubsystem();
			return;
		}
		checkPossibilityFitSubsystem(subsystem);
		setAttackSubsystem(subsystem);
	}

	private void setAttackSubsystem(AttackSubsystem subsystem) {
		changePowergrid(-subsystem.getPowerGridConsumption().value());
		this.attackSubsystem = subsystem;
	}

	private void removeAttackSubsystem() {
		changePowergrid(this.attackSubsystem.getPowerGridConsumption().value());
		this.attackSubsystem = null;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			removeDefenciveSubsystem();
			return;
		}
		checkPossibilityFitSubsystem(subsystem);
		setDefenciveSubsystem(subsystem);
	}

	private void setDefenciveSubsystem(DefenciveSubsystem subsystem) {
		changePowergrid(-subsystem.getPowerGridConsumption().value());
		this.defenciveSubsystem = subsystem;
	}

	private void removeDefenciveSubsystem() {
		changePowergrid(this.defenciveSubsystem.getPowerGridConsumption().value());
		this.defenciveSubsystem = null;
	}

	private void changePowergrid(Integer value) {
		this.powergridOutput = PositiveInteger.of(this.powergridOutput.value() + value);
	}

	protected void checkPossibilityFitSubsystem(Subsystem subsystem) throws InsufficientPowergridException {
		int shortage = this.powergridOutput.value() - subsystem.getPowerGridConsumption().value();
		if (shortage < 0) {
			throw new InsufficientPowergridException(shortage);
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		checkIsAllSubsystemsFitted();
		return new CombatReadyShip(this);
	}

	protected void checkIsAllSubsystemsFitted() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem != null && this.attackSubsystem != null) {
			return;
		}
		throw (this.defenciveSubsystem == null && this.attackSubsystem == null ? NotAllSubsystemsFitted.bothMissing()
				: (this.defenciveSubsystem == null ? NotAllSubsystemsFitted.defenciveMissing()
						: NotAllSubsystemsFitted.attackMissing()));
	}

	protected boolean changeCapacitorAmount(int value) {
		int newCapacitorAmount = this.capacitorAmount.value() + value;
		if (newCapacitorAmount < 0) {
			return false;
		}
		setCapacitorAmount(PositiveInteger.of(newCapacitorAmount));
		return true;
	}

	protected PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	protected PositiveInteger getSize() {
		return this.size;
	}

	protected AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	protected void setCapacitorAmount(PositiveInteger capacitorAmount) {
		this.capacitorAmount = capacitorAmount;
	}

	protected String getName() {
		return this.name;
	}

	protected void capacitorRecharge() {
		this.capacitorAmount = PositiveInteger.of(Math.min(this.capacitorAmountMax.value(),
				this.capacitorAmount.value() + this.capacitorRechargeRate.value()));
	}

	protected Optional<RegenerateAction> shieldRegenerate() {
		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		PositiveInteger shieldRegenerated = this.shieldHP.value() < this.shieldHPMax.value()
				? regenerateAction.shieldHPRegenerated : PositiveInteger.of(0);
		PositiveInteger hullRegenerated = this.hullHP.value() < this.hullHPMax.value()
				? regenerateAction.shieldHPRegenerated : PositiveInteger.of(0);
		return Optional.of(new RegenerateAction(shieldRegenerated, hullRegenerated));
	}

	public AttackResult applyAttack(AttackAction attack) {
		AttackAction attackActionReduced = this.defenciveSubsystem.reduceDamage(attack);
		int hullHPNew = this.hullHP.value();
		int shieldHPNew = this.shieldHP.value() - attackActionReduced.damage.value();
		if (shieldHPNew < 0) {
			hullHPNew += shieldHPNew;
			shieldHPNew = 0;
		}
		if (hullHPNew <= 0) {
			return new AttackResult.Destroyed();
		}
		this.hullHP = PositiveInteger.of(hullHPNew);
		this.shieldHP = PositiveInteger.of(shieldHPNew);
		return new AttackResult.DamageRecived(attackActionReduced.weapon, attackActionReduced.damage,
				this.defenciveSubsystem);
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

}
